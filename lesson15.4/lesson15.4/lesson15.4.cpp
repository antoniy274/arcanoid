﻿#include <iostream>

void FindOddNumbers(int Limit, bool IsOdd);

int main()
{
	int i{ 0 };
	int N;
	std::cout << "Enter number ";
	std::cin >> N;
	while (i < N) {
		if (i % 2 == 0) {
			std::cout << i << "\n";
		}
		++i;
	}
	bool IsOdd = true;
	FindOddNumbers(N, IsOdd);
}

void FindOddNumbers(int Limit, bool IsOdd) {
	for (int i = IsOdd; i <= Limit; i += 2) {
		std::cout << i << "\n";
	}
}