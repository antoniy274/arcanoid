// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestSnakeGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TESTSNAKEGAME_API ATestSnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
