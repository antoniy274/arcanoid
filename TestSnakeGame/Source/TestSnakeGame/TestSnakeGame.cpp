// Copyright Epic Games, Inc. All Rights Reserved.

#include "TestSnakeGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TestSnakeGame, "TestSnakeGame" );
