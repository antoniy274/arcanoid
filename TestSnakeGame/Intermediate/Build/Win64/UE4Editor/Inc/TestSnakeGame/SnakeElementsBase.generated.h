// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTSNAKEGAME_SnakeElementsBase_generated_h
#error "SnakeElementsBase.generated.h already included, missing '#pragma once' in SnakeElementsBase.h"
#endif
#define TESTSNAKEGAME_SnakeElementsBase_generated_h

#define TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_SPARSE_DATA
#define TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_RPC_WRAPPERS
#define TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeElementsBase(); \
	friend struct Z_Construct_UClass_ASnakeElementsBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeElementsBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TestSnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElementsBase)


#define TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASnakeElementsBase(); \
	friend struct Z_Construct_UClass_ASnakeElementsBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeElementsBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TestSnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElementsBase)


#define TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeElementsBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeElementsBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElementsBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElementsBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElementsBase(ASnakeElementsBase&&); \
	NO_API ASnakeElementsBase(const ASnakeElementsBase&); \
public:


#define TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElementsBase(ASnakeElementsBase&&); \
	NO_API ASnakeElementsBase(const ASnakeElementsBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElementsBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElementsBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeElementsBase)


#define TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_PRIVATE_PROPERTY_OFFSET
#define TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_11_PROLOG
#define TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_PRIVATE_PROPERTY_OFFSET \
	TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_SPARSE_DATA \
	TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_RPC_WRAPPERS \
	TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_INCLASS \
	TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_PRIVATE_PROPERTY_OFFSET \
	TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_SPARSE_DATA \
	TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_INCLASS_NO_PURE_DECLS \
	TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTSNAKEGAME_API UClass* StaticClass<class ASnakeElementsBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestSnakeGame_Source_TestSnakeGame_SnakeElementsBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
