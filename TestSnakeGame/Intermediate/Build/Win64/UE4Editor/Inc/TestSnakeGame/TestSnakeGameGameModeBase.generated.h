// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTSNAKEGAME_TestSnakeGameGameModeBase_generated_h
#error "TestSnakeGameGameModeBase.generated.h already included, missing '#pragma once' in TestSnakeGameGameModeBase.h"
#endif
#define TESTSNAKEGAME_TestSnakeGameGameModeBase_generated_h

#define TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_SPARSE_DATA
#define TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_RPC_WRAPPERS
#define TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATestSnakeGameGameModeBase(); \
	friend struct Z_Construct_UClass_ATestSnakeGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ATestSnakeGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TestSnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ATestSnakeGameGameModeBase)


#define TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATestSnakeGameGameModeBase(); \
	friend struct Z_Construct_UClass_ATestSnakeGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ATestSnakeGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TestSnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ATestSnakeGameGameModeBase)


#define TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATestSnakeGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATestSnakeGameGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATestSnakeGameGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATestSnakeGameGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATestSnakeGameGameModeBase(ATestSnakeGameGameModeBase&&); \
	NO_API ATestSnakeGameGameModeBase(const ATestSnakeGameGameModeBase&); \
public:


#define TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATestSnakeGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATestSnakeGameGameModeBase(ATestSnakeGameGameModeBase&&); \
	NO_API ATestSnakeGameGameModeBase(const ATestSnakeGameGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATestSnakeGameGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATestSnakeGameGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATestSnakeGameGameModeBase)


#define TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_12_PROLOG
#define TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_SPARSE_DATA \
	TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_RPC_WRAPPERS \
	TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_INCLASS \
	TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_SPARSE_DATA \
	TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTSNAKEGAME_API UClass* StaticClass<class ATestSnakeGameGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestSnakeGame_Source_TestSnakeGame_TestSnakeGameGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
