﻿#include <iostream>

int main()
{
    std::string city{ "Hamburg" };
    std::cout << "City = " << city << "\n";
    std::cout << "Variable city length " << city.length() << "\n";
    std::cout << "First symbol " << city[0] << " ,last symbol " << city[city.size() - 1] << "\n";
}
