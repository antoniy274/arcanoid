#include "Vector.h"
#include <iostream>
#include <math.h>

Vector::Vector(int b, int e)
{
	begin = b;
	end = e;
}

void Vector::show_begin()
{
	if (begin < end) {
		std::cout << begin << '\n';
	}
	else {
		std::cout << end << '\n';
	}
}

void Vector::show_end()
{
	if (begin > end) {
		std::cout << begin << '\n';
	}
	else {
		std::cout << end << '\n';
	}
}

void Vector::show_abs()
{
	std::cout << std::abs(begin - end) << '\n';
}

void Vector::show_lenght()
{
	std::cout << std::sqrt(pow(begin, 2) + pow(end, 2));
}
