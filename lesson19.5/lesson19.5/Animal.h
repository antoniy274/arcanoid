#include<string>
#pragma once
class Animal
{
	//std::string name;
public:
	Animal() {};
	virtual void Voice();
};

class Dog : public Animal {
	//std::string name;
public:
	Dog() {};
	void Voice();
};

class Cat : public Animal {
	//std::string name;
public:
	Cat() {};
	void Voice();
};

class Snake : public Animal {
	//std::string name;
public:
	Snake() {};
	void Voice();
};
