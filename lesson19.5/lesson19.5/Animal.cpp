#include<iostream>
#include "Animal.h"

void Animal::Voice() {
	std::cout << "Voice\n";
}

void Dog::Voice() {
	std::cout << "Woof\n";
}

void Cat::Voice() {
	std::cout << "Meow\n";
}

void Snake::Voice() {
	std::cout << "Shhhhh\n";
}