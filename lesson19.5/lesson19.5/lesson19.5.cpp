﻿#include <iostream>
#include "Animal.h"

int main()
{
    Cat* cat = new Cat();
    Dog* dog = new Dog;;
    Snake* snake = new Snake();
    Animal* animal[3] = { cat, dog, snake };

    for (int i = 0; i < 3; i++) {
        animal[i]->Voice();
    }
    delete cat;
    delete dog;
    delete snake;
}