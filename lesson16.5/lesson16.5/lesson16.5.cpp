﻿#include <iostream>
#include <vector>
#include <time.h>


int main()
{
    const int N = 5;
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    buf.tm_mday;
    std::cout << buf.tm_mday % N << "\n";
    
    int arr[N][N];
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            arr[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            std::cout << arr[i][j] << ' ';
        }
        std::cout << "\n";
    }

    int sum = 0;
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            if (j == buf.tm_mday % N) {
                sum += arr[i][j];
            }
        }
    }
    std::cout << sum << "\n";
}