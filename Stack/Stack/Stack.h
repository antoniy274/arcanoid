#pragma once

typedef unsigned long Item;

class Stack
{
private:
	enum { MAX = 10 };
	Item items[MAX];
	int top;
public:
	Stack();
	bool IsEmpty() const;
	bool IsFull() const;
	bool push(const Item& item);
	bool pop(Item& item);
};