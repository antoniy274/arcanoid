// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGemeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGEME_API ASnakeGemeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
