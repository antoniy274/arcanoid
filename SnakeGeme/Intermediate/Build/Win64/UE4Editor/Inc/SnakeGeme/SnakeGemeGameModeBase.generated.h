// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGEME_SnakeGemeGameModeBase_generated_h
#error "SnakeGemeGameModeBase.generated.h already included, missing '#pragma once' in SnakeGemeGameModeBase.h"
#endif
#define SNAKEGEME_SnakeGemeGameModeBase_generated_h

#define SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_SPARSE_DATA
#define SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_RPC_WRAPPERS
#define SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeGemeGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGemeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGemeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGeme"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGemeGameModeBase)


#define SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeGemeGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGemeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGemeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGeme"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGemeGameModeBase)


#define SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGemeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGemeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGemeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGemeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGemeGameModeBase(ASnakeGemeGameModeBase&&); \
	NO_API ASnakeGemeGameModeBase(const ASnakeGemeGameModeBase&); \
public:


#define SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGemeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGemeGameModeBase(ASnakeGemeGameModeBase&&); \
	NO_API ASnakeGemeGameModeBase(const ASnakeGemeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGemeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGemeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGemeGameModeBase)


#define SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_12_PROLOG
#define SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_SPARSE_DATA \
	SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_RPC_WRAPPERS \
	SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_INCLASS \
	SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_SPARSE_DATA \
	SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGEME_API UClass* StaticClass<class ASnakeGemeGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGeme_Source_SnakeGeme_SnakeGemeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
